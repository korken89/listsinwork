# Short intro to ROS with catkin and master synchronization

## Consisting parts
* Installing catkin_tools
* What to source
* How to set parameters for debug/release
* Problems with using lambdas
* ROS master synchronization using node_manager
