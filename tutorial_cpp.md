# Short intro to C++ @ medium level


## Consisting parts

* Never use pointers
* Classes
* STL containers
* Templates
    * Interfaces
    * Couriously Reoccuring Templates
* Threading
    * Mutexes (resource locks)
    * Condition variables
    * Starting a thread
    * Async
    * Future
* Project structure
* CMake?
