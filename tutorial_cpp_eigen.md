# Short intro to C++ and Eigen


## Consisting parts

* Double vs Float
* Static math
    * Matrix
    * Vectors
    * Fixed-size vectorizable matrices and vectors
* Dynamic math
    * Matrix
    * Vectors
* Standard operations
    * Add
    * Mul
    * Transpose
    * Norms
    * Inverses
    * ...
* Matrix rotations using quaternions
* Eigen types in classes
    * http://eigen.tuxfamily.org/dox/group__TopicStructHavingEigenMembers.html
* Eigen types in STL containers
    * http://eigen.tuxfamily.org/dox/group__TopicStlContainers.html
* Memory allocation boundaries
* Eigen and parallelization
    * http://eigen.tuxfamily.org/dox/TopicMultiThreading.html
* Eigen and templating
    * http://eigen.tuxfamily.org/dox/TopicTemplateKeyword.html
