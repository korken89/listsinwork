# Short intro to optimization in C++ with NLopt

## Consisting parts

* NLopt example usage
* How to pass parameters
* Show the problem with lambda functions
* Properties of algorithms
    * Local vs global
    * Derivative free vs with derivative
